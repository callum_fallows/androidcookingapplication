package uk.co.fallows.callum.learntocookapplication.models;

import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class Recipe {

    public String Image;
    public String Title;
    public String Text;
    public String Serves;
    public String PrepTime;
    public String CookingTime;
    public String[] Ingredients;
    public String[] Method;

    public static String[] toStringArray(JSONArray array) {
        if(array==null)
            return null;

        String[] arr=new String[array.length()];
        for(int i=0; i<arr.length; i++) {
            arr[i]=array.optString(i);
        }
        return arr;
    }

    public Recipe(JSONObject obj) {

        try {


            this.Image = obj.getString("Image");
            this.Title = obj.getString("Title");
            this.Text = obj.getString("Text");
            this.Serves = obj.getString("Serves");
            this.PrepTime = obj.getString("PrepTime");
            this.CookingTime = obj.getString("CookingTime");
            this.Ingredients = toStringArray(obj.getJSONArray("Ingredients"));
            this.Method = toStringArray(obj.getJSONArray("Method"));

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public Recipe() {

    }



}