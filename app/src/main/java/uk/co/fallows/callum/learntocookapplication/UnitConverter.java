package uk.co.fallows.callum.learntocookapplication;

import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import java.util.Objects;

public class UnitConverter extends MainActivity {
    private EditText mFromField;
    private Button convertButton;
    private TextView mToField;
    private Spinner mDropDown1;
    private Spinner mDropDown2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_unit_converter);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        navigationView.getMenu().getItem(1).setChecked(true);

        mFromField = findViewById(R.id.unit1);
        mToField = findViewById(R.id.unit2);
        convertButton = findViewById(R.id.convertButton);
        mDropDown1 = findViewById(R.id.dropDownList1);
        mDropDown2 = findViewById(R.id.dropDownList2);

        String[] items = new String[]{"g", "oz"};

        ArrayAdapter<String> adapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_dropdown_item, items);
        mDropDown1.setAdapter(adapter);
        mDropDown2.setAdapter(adapter);
        convertUnitButton();
        setDefaultSpinner();

    }
    public void setDefaultSpinner() {
        String myString = "oz"; //the value you want the position for

        ArrayAdapter myAdap = (ArrayAdapter) mDropDown2.getAdapter(); //cast to an ArrayAdapter

        int spinnerPosition = myAdap.getPosition(myString);

        //set the default according to value
        mDropDown2.setSelection(spinnerPosition);
    }
    public void updateUnitConverter() {
        String selectedUnit = mDropDown1.getSelectedItem().toString();
        String selectedUnit2 = mDropDown2.getSelectedItem().toString();

        if (!Objects.equals(mFromField.getText().toString(), "")) {
            double newValue = Double.parseDouble(mFromField.getText().toString());


            if (!Objects.equals(selectedUnit, selectedUnit2)) {
                if (Objects.equals(selectedUnit, "g") && Objects.equals(selectedUnit2, "oz")) {
                    mToField.setText(String.valueOf(gramsToOunces(newValue)));
                }

                if (Objects.equals(selectedUnit, "oz") && Objects.equals(selectedUnit2, "g")) {
                    mToField.setText(String.valueOf(ouncesToGrams(newValue)));
                }
            }
        }
    }
    public void convertUnitButton() {
        convertButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                // Code here executes on main thread after user presses button
                updateUnitConverter();
            }
        });
    }
    public double gramsToOunces(double grams) {
        return grams / 28.35;
    }
    public double ouncesToGrams(double ounces) {
        return ounces * 28.35;
    }

}
