package uk.co.fallows.callum.learntocookapplication;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;

public class MainActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // Make sure this is before calling super.onCreate
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        navigationView.getMenu().getItem(0).setChecked(true);
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_home) {
            Intent intent = new Intent(this, MainActivity.class);
            startActivity(intent);
        } else if (id == R.id.nav_cookery_terms) {
            Intent intent = new Intent(this, CookeryTerms.class);
            startActivity(intent);
        } else if (id == R.id.nav_unit_converter) {
            Intent intent = new Intent(this, UnitConverter.class);
            startActivity(intent);
        } else if (id == R.id.nav_recipes) {
            Intent intent = new Intent(this, Recipes.class);
            startActivity(intent);
        } else if (id == R.id.nav_my_recipes) {
            Intent intent = new Intent(this, MyRecipes.class);
            startActivity(intent);
        } else if (id == R.id.nav_kitchen_management) {
            Intent intent = new Intent(this, KitchenManagement.class);
            startActivity(intent);
        } else if (id == R.id.nav_kitchen_equipment) {
            Intent intent = new Intent(this, KitchenEquipment.class);
            startActivity(intent);
        } else if (id == R.id.nav_quiz) {
           Intent intent = new Intent(this, Quiz.class);
            startActivity(intent);
        } else if (id == R.id.nav_cookery_terms) {
            Intent intent = new Intent(this, CookeryTerms.class);
            startActivity(intent);
        } else if (id == R.id.nav_essential_tips) {
            Intent intent = new Intent(this, TopTips.class);
            startActivity(intent);
        }

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }
}
