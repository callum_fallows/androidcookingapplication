package uk.co.fallows.callum.learntocookapplication;
import android.content.ContentValues;
import android.content.Intent;
        import android.database.Cursor;
        import android.database.sqlite.SQLiteDatabase;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.design.widget.NavigationView;
import android.support.v4.widget.DrawerLayout;
        import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ListView;
        import android.widget.SimpleCursorAdapter;


import uk.co.fallows.callum.learntocookapplication.helpers.Contract;
import uk.co.fallows.callum.learntocookapplication.helpers.DBHelper;

public class MyRecipes extends MainActivity implements AdapterView.OnItemClickListener {

    private EditText mName;
    private EditText mText;

    private ListView mList;
    private DBHelper mHelper;

    private SQLiteDatabase mDB;

    private Cursor mCursor;

    private SimpleCursorAdapter mAdapter;


    @RequiresApi(api = Build.VERSION_CODES.CUPCAKE)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_recipes);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        navigationView.getMenu().getItem(6).setChecked(true);

        // grabs the edit text fields in the UI
        mName = findViewById(R.id.edit_name);
        mText = findViewById(R.id.edit_height);

        // sets up the list view with an item-click event handler
        mList = findViewById(R.id.lvMyRecipes);
        mList.setOnItemClickListener(this);

        // initialises the DB if it doesn't exist
        mHelper = new DBHelper(this);

    }


    @RequiresApi(api = Build.VERSION_CODES.HONEYCOMB)
    @Override
    public void onResume()
    {
        super.onResume();

        // gets a connection to the DB
        mDB = mHelper.getWritableDatabase();

        // cals the convenience method getAll() which grabs the whole table
        mCursor = mHelper.getAll(mDB);

        // connects the list to the data - these are the two fields to view
        String[] headers = new String[]{Contract.Example.COLNAME_NAME,
                Contract.Example.COLNAME_TEXT};

        // creates an adapter from a really useful stock Android
        // built-in layout. Note the use of the built-in
        // "android.R.id...", not this app's "R.id..."
        mAdapter = new SimpleCursorAdapter(this,
                R.layout.single_recipe,
                mCursor, headers, new int[]{R.id.Title,
                R.id.Text}, 0);

        // links the adapter to the list viewer
        mList.setAdapter(mAdapter);
    }

    public void onClick(View v)
    {
        // gets the name, height data from the UI
        String name = mName.getText().toString();
        String text = mText.getText().toString();

        mName.setText("");
        mText.setText("");

        // adds the record to the DB
        ContentValues cv = new ContentValues(2);
        cv.put(Contract.Example.COLNAME_NAME, name);
        cv.put(Contract.Example.COLNAME_TEXT, text);
        mDB.insert(Contract.Example.TABLE_NAME, null, cv);

        // the list has changed, so we query the DB and
        // give the adapter an updated Cursor of data
        mCursor = mHelper.getAll(mDB);
        mAdapter.changeCursor(mCursor);

        // forces the updated list to refresh the display
        mAdapter.notifyDataSetChanged();
    }

    @Override
    public void onPause()
    {
        super.onPause();
        mDB.close();
        mCursor.close();
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View v, int position, long id)
    {
        // moves to the position in the DB given by the listview click
        mCursor.moveToPosition(position);

        // gets the name, height data from the UI
        final String name = mCursor.getString(mCursor.getColumnIndex("name"));
        final String text = mCursor.getString(mCursor.getColumnIndex("text"));

        Intent intent = new Intent(MyRecipes.this, SingleRecipe.class);
        intent.putExtra("Title", name);
        intent.putExtra("Text", text);

        startActivity(intent);

    }



}